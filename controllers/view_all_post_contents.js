var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post  = mongoose.model('skug_post');
var skug_post_seo  = mongoose.model('skug_post_seo');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");

	//@ Show Data when  post Name call----------------------> 
exports.post_page_description  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var p_id = qdata.pid
	var post_url = qdata.post_url;
	skug_post.find({post_url: post_url},(function(err, result) {
	    if(err) res.json(err);
		    skug_post.find({},(function(err, page_content) {
		    if(err) res.json(err);
		    res.render('backend/post-page-description', {postdata: result, pagename: page_content, active_user_details: active_user_details, page_url: page_url});
		}));
	}));
};

	//@	View all post data here------------------------------>
exports.view_all_post_data = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	skug_post.find({},(function(err, page_content) {
	    if(err) res.json(err);
	    	res.render('backend/view_all_post', {post_page: page_content, active_user_details: active_user_details, page_url: page_url});
	}));
}