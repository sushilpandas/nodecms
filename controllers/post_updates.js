var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post  = mongoose.model('skug_post');
var skug_post_seo  = mongoose.model('skug_post_seo');
var skug_post_category  = mongoose.model('skug_post_category');
var skug_category_relation  = mongoose.model('skug_category_relation');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
//console.log('storage ' + Storage);
var upload = multer({
     	storage: Storage
 	}).any();
// .array("imgUploader", 100);
 	 //Field name and max count
 	 var post_nme;
exports.post_update = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	// var o_id = new ObjectId(req.params._id);
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var p_id = qdata.pid
     post_nme = qdata.pname; 
    // console.log("postName01 is here *****************************************");
    var cate_name = new Array();
    var post_selected_category = new Array();
    var selcted_post_category = new Array();
    var category_concat = new Array();

    // @ find one record post and end on backend/post-update.
    skug_post.findOne({"_id":ObjectId(p_id)},(function(err, result) {
	    	skug_post_seo.findOne({post_name: post_nme},(function(err, post_seo) {
		    	skug_category_relation.find({"postName":post_nme},(function(err, results) {
		    		if (err) throw err;
		    		for(var_in in results){
		    			selcted_post_category.push(results[var_in].post_category);
		    			// console.log( results[var_in].post_category);
		    		}
				if ( selcted_post_category ) {
					skug_post_category.find({},(function(err, category_data){
						if (err) throw err;
						
						for(val in category_data)
						{
							cate_name.push(category_data[val].category_name);
						}
						const post = cate_name;
						for(val in cate_name ) {
							for(val_1 in selcted_post_category) {
								if(selcted_post_category[val_1] == cate_name[val]) {
									// console.log(result.post_category[val_1] + ' index is '+ val_1);
									post.splice(val, 1);
								} 
							}
						}
							post_selected_category = selcted_post_category;
							category_concat = cate_name.concat(post_selected_category);
							var category_filter_result = category_concat.filter(function (item, pos) {
								return category_concat.indexOf(item) == pos});
					    res.render('backend/post-update', {unselected_category: post,post_data:result,post_selected_category_result:post_selected_category, active_user_details: active_user_details,  page_url: page_url, post_seo: post_seo});
					}));
				}
				else{
						// @ find all category send on backend/post-update page.
					skug_post_category.find({},(function(err, category_data){
						if (err) throw err;	
						res.render('backend/post-update', {unselected_category: category_data,post_data:result,post_selected_category_result:post_selected_category, active_user_details: active_user_details,  page_url: page_url, post_seo: post_seo});
					}));	
				}
			}));
		}));
	}));

}
//Update post All data here Operation---------------------------->
exports.post_data_update   = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	//@ Upload image begins here...
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
        var updateTime = new Date();
		    // Create a mongo query with all form parameters to be inserted
		    if(imgfile.length != 0){
			// update form parameters in the collection "Employee" using above query named "user"
		  	skug_post.updateOne({"_id":ObjectId(req.body.id)} ,{$set:{
		  		postName: req.body.post_name, 
				post_url: req.body.post_url,
				featured_imaeg: req.files[0].filename,
				contents: req.body.content, 
				post_description: req.body.message,
				gallery: imgfile,
				publish : req.body.publish,
				UpdateDate: updateTime}
			}, function(err, res) {
				console.log(" req.body.post_name 02 ");
				console.log(req.body.post_name);
			  		skug_post_seo.updateOne({post_name :req.body.post_names} ,{$set:
			  		{
			  		post_name: req.body.post_name,
			  		seo_title: req.body.seo_title,
					seo_descriptions: req.body.seo_description,
					seo_keywords: req.body.seo_keywords,
					seo_canonicals: req.body.seo_canonical,
					og_title: req.body.og_title,
					og_descriptions: req.body.og_description,
					og_urls: req.body.og_url,
					og_types: req.body.og_type,
					og_images: req.body.og_image,
					og_site_names: req.body.og_site_name,
					twitter_titles: req.body.twitter_title,
					twitter_descriptions: req.body.twitter_description,
					twitter_cards: req.body.twitter_card,
					twitter_sites: req.body.twitter_site,
					twitter_creators: req.body.twitter_creator }
				}, function(err, res) {
		    	// If error arises, i.e. data not inserted
		    	if (err) throw err;
		    	// else on succes
		    	console.log("1 document Update with image");
		  		}); // Insert query ends here.
		  	});
		}
		else{
			// update form parameters in the collection "Employee" using above query named "user"
		  	skug_post.updateOne({"_id":ObjectId(req.body.id)} ,{$set: {
		  		postName: req.body.post_name, 
				post_url: req.body.post_url,
				contents: req.body.content, 
				post_description: req.body.message,
				publish : req.body.publish,
				UpdateDate: updateTime
			}
			}, function(err, res) {
			  		skug_post_seo.updateOne({post_name :req.body.post_names} ,{$set: {
			  		post_name: req.body.post_name,
			  		seo_title: req.body.seo_title,
					seo_descriptions: req.body.seo_description,
					seo_keywords: req.body.seo_keywords,
					seo_canonicals: req.body.seo_canonical,
					og_title: req.body.og_title,
					og_descriptions: req.body.og_description,
					og_urls: req.body.og_url,
					og_types: req.body.og_type,
					og_images: req.body.og_image,
					og_site_names: req.body.og_site_name,
					twitter_titles: req.body.twitter_title,
					twitter_descriptions: req.body.twitter_description,
					twitter_cards: req.body.twitter_card,
					twitter_sites: req.body.twitter_site,
					twitter_creators: req.body.twitter_creator }
				}, function(err, res) {
			    	// If error arises, i.e. data not inserted
			    	if (err) throw err;
			    	// else on succes
			    	console.log("1 document Update without image");
			  	}); // Insert query ends here
		  	});
		}
		// Return a success message if image inserted properly.
		skug_category_relation.find({"postName":post_nme},(function(err, select_category) {
		if (err) throw err;
		console.log("select_category data");
		console.log(select_category.length);
		if (select_category.length !== 0) {
			for(val_select_del_cat in select_category){
				skug_category_relation.deleteOne({"postName":post_nme}, (function(err, result) {
				    if(err) res.json(err);
				    console.log("skug_post_category Deleted Successfully")
				}));
			}
			for(val_up_cat in req.body.category){
				console.log("val_up_cat in req.body.category");
				console.log(req.body.category[val_up_cat]);
				skug_category_relation.insertMany({
						postName: req.body.post_name, 
						post_url: req.body.post_url,
						post_category: req.body.category[val_up_cat],
						post_featured_imaeg: req.body.featured_image
					}, 
					function(err, res) {
						if (err) throw err;
						    	console.log("category Relation data inserted document inserted");
					}
				);
			}
		}
			else{
				for(val_up_cat1 in req.body.category){
					console.log("val_up_cat in req.body.category");
					console.log(req.body.category[val_up_cat1]);
					skug_category_relation.insertMany({
					postName: req.body.post_name,
					post_url: req.body.post_url,
					post_category: req.body.category[val_up_cat1],
					post_featured_imaeg: req.body.featured_image
				}, function(err, res) {
					if (err) throw err;
					    	console.log("category Relation data inserted document inserted");
					});
				}
			}
		}));
	});
  	skug_post.find({},(function(err, postdata) {
	    if(err) res.json(err);
	    res.render('backend/view_all_post', {post_page: postdata, active_user_details: active_user_details,  page_url: page_url});
	}));
};