var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post_category  = mongoose.model('skug_post_category');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var c_id;
	//@for render on category update page export here. 
exports.edit_post_category_page = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	// var o_id = new ObjectId(req.params._id);
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
    c_id = qdata.cid
    skug_post_category.findOne({"_id":ObjectId(c_id)},(function(err, result) {
			if (err) throw err;
		  res.render('backend/edit-post-category', {post_category_data: result, active_user_details: active_user_details, page_url: page_url});
	}));
};
	// @ Update category data on clickin on submit button.
exports.edit_post_category_submit = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
		//# Find Post Id and Post name Here...
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	skug_post_category.updateOne({"_id":ObjectId(req.body.cid)} ,{$set:
	 	{
		        category_name                : req.body.category_name,
		        category_slug                : req.body.category_slug,
		        category_description         : req.body.category_description,
		        cat_seo_title                : req.body.cat_seo_title,
		        cat_seo_descriptions         : req.body.cat_seo_description,
		        cat_seo_keywords             : req.body.cat_seo_keywords,
		        cat_seo_canonicals           : req.body.cat_seo_canonical,
		        cat_og_title                 : req.body.cat_og_title,
		        cat_og_descriptions          : req.body.og_description,
		        cat_og_urls                  : req.body.cat_og_url,
		        cat_og_types                 : req.body.cat_og_type,
		        cat_og_images                : req.body.cat_og_image,
		        cat_og_site_names            : req.body.cat_og_site_name,
		        cat_twitter_titles           : req.body.cat_twitter_title,
		        cat_twitter_descriptions     : req.body.twitter_description,
		        cat_twitter_cards            : req.body.cat_twitter_card,
		        cat_twitter_sites            : req.body.cat_twitter_site,
		        cat_twitter_creators         : req.body.cat_twitter_creator
		    	}
			}, function(err, res) {
		  		if(err) res.json(err);
		  		console.log("successfull");
	});
	//@ Find post Query  Here------------------>
    skug_post_category.find({},(function(err, post_category) {
	    if(err) res.json(err);
	    res.render('backend/view-all-post-category', {post_category: post_category, active_user_details: active_user_details, page_url: page_url});
	}));
};
