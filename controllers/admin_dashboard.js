// var http        = require("http");
var express     = require("express");
var app         = express();
var path        = require("path");
var url_exp         = require('url');

//@Admin Dashboard here------------------------>
exports.admin_dashboard = function(req,res) {
	// console.log(req);
	var active_user_details = req.session.user;
	res.render('backend/admin-dashboard',{active_user_details: active_user_details});
}