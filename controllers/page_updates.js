var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_page  = mongoose.model('skug_page');
var skug_page_seo  = mongoose.model('skug_page_seo');
var skug_site_settings  = mongoose.model('skug_site_settings');
var banner_details  = mongoose.model('banner_details');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var all_banner_pages = [];
var banner_pages;
var templete_name;

var theme_name;

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
//console.log('storage ' + Storage);
var upload = multer({
    storage: Storage
}).any();
exports.page_update = function(req,res){
  var all_templets = [];
  var page_url = req.originalUrl;
  var active_user_details = req.session.user;
  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  var q = url_exp.parse(fullUrl, true);
  var qdata = q.query; 
  var p_id = qdata.pid;
  var page_name = qdata.pname; 
  	getSiteSettingsDetails((site_settings) => {
		for(val in site_settings){
			theme_name = site_settings[val].theam_name;
		}
		skug_page.find({"_id":ObjectId(p_id)},(function(err, result) {
			for(value in result){
				templete_name = result[value].page_template;
			}
			var dir = './public/frontend/theme/'+ theme_name;
			fs.readdir(dir, function (err, all_templet) {
			    //handling error
			    if (err) {
			        return console.log('Unable to scan directory: ' + err);
			    }
			    for(value in all_templet ){
			    	var arr = all_templet[value];
			    	if(arr.includes(".ejs") == true){
			    		all_templets.push(arr);
			    	}
			    }
			    const templetes = all_templets.indexOf(templete_name);
				if (templetes > -1) {
				  all_templets.splice(templetes, 1);
				}
			});
			skug_page_seo.findOne({ page_name: page_name }, function(err, page_seo_data){
		    if(err) res.json(err);
		    	for(val in result){
		    	banner_pages = result[val].banner_page;
		    	}
		    	banner_details.find({},(function(err,  all_banner) {
			    	if(err) res.json(err);
			    	for(value in all_banner ){
			    	 all_banner_pages.push(all_banner[value].banner_title);
			    	}
			    	const all_themes = all_banner_pages.indexOf(banner_pages);
						if (all_themes > -1) {
						  all_banner_pages.splice(all_themes, 1);
						}
						console.log("all_templets");
						console.log(all_templets);
				    res.render('backend/page-update', {all_banner: all_banner, pagedata: result, page_seo_data: page_seo_data, active_user_details: active_user_details,  page_url: page_url, all_banner_pages: all_banner_pages,all_templets: all_templets});
				}));
	    	});
		}));
	});
};
	// @page update here export .
exports.pageUpdate_content   = function(req,res) {
	console.log(req.body);
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	//@ Upload image begins here...
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
        var updateTime = new Date();
		    console.log(imgfile);
			if(imgfile != null){
			// update form parameters in the collection "Employee" using above query named "user"
		  	skug_page.updateOne({"_id":ObjectId(req.body.id)} ,{$set:
		  		{ 	
					pageName: req.body.pageName, 
					URL: req.body.pageUrl,
					featuredImaeg: req.files[0].filename,
					contents: req.body.content, 
					message: req.body.message,
					gallery: imgfile,
					banner_page : req.body.banner_page,
					UpdateDate: updateTime,
					page_template : req.body.page_template,
					publish: req.body.publish
				}
			}, function(err, res) {
		  		skug_page_seo.updateOne({page_name : req.body.page_name} ,{$set:
		  			{
		  			page_name: req.body.pageName,
					seo_title: req.body.seo_title,
					seo_descriptions: req.body.seo_description,
					seo_keywords: req.body.seo_keywords,
					seo_canonicals: req.body.seo_canonical,
					og_title: req.body.og_title,
					og_descriptions: req.body.og_description,
					og_urls: req.body.og_url,
					og_types: req.body.og_type,
					og_images: req.body.og_image,
					og_site_names: req.body.og_site_name,
					twitter_titles: req.body.twitter_title,
					twitter_descriptions: req.body.twitter_description,
					twitter_cards: req.body.twitter_card,
					twitter_sites: req.body.twitter_site,
					twitter_creators: req.body.twitter_creator
				}
			}, function(err, page_seo) {
		    	// If error arises, i.e. data not inserted
		    	if (err) throw err;
		    	// else on succes
		    	console.log("1 document Update");
		  	}); // Updates query ends here.
		  	});
		  	}
			else{
				// update form parameters in the collection "Employee" using above query named "user"
			  	skug_page.updateOne({"_id":ObjectId(req.body.id)} ,{$set:
			  		{
			  		pageName: req.body.pageName, 
					URL: req.body.pageUrl,
					// featuredImaeg: req.files[0].filename,
					contents: req.body.content, 
					message: req.body.message,
					banner_page : req.body.banner_page,
					UpdateDate: updateTime,
					page_template : req.body.page_template,
					publish: req.body.publish
				}
			  }, function(err, res) {
			  		skug_page_seo.updateOne({page_name : req.body.page_name} ,{$set:
			  		{	
			  			page_name: req.body.pageName,
						seo_title: req.body.seo_title,
						seo_descriptions: req.body.seo_description,
						seo_keywords: req.body.seo_keywords,
						seo_canonicals: req.body.seo_canonical,
						og_title: req.body.og_title,
						og_descriptions: req.body.og_description,
						og_urls: req.body.og_url,
						og_types: req.body.og_type,
						og_images: req.body.og_image,
						og_site_names: req.body.og_site_name,
						twitter_titles: req.body.twitter_title,
						twitter_descriptions: req.body.twitter_description,
						twitter_cards: req.body.twitter_card,
						twitter_sites: req.body.twitter_site,
						twitter_creators: req.body.twitter_creator
					}
			}, function(err, page_seo) {
			    	// If error arises, i.e. data not inserted
			    	if (err) throw err;
			    	// else on succes
			    	console.log("1 document Update");

			  	}); //skug_page Update query ends here.
			  	});//skug_page_seo update query ends here.
			}
	}); // Upload image begins here...
    	skug_page.find({},(function(err, pagedata) {
		    if(err) res.json(err);
			// console.log(admin_profile);
		    res.render('backend/view_all_pages', {pageData_content: pagedata, active_user_details: active_user_details, page_url: page_url});
		}));
};


/////////////////////////////////////////////////////////////////
///                       	functions						 ///
///////////////////////////////////////////////////////////////

// @------------------------------------------------// Start -------//
// callback fucton for fetch site settings details.
	function getSiteSettingsDetails(callback) {
		skug_site_settings.find({},(function(err,  site_settings) {
		    if(err)
		        callback(err)
		    else 
		        callback(site_settings); 
		}));
	}
// @----------------------------------------------------// End -----//]