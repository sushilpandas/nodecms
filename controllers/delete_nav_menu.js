var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_nav_menu  = mongoose.model('skug_nav_menu');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
var nav_id;

	//@delete nav menues on clicking delete button.  
exports.delete_nav_menu_page = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	nav_id = qdata.nid;
	skug_nav_menu.deleteOne({"_id":ObjectId(nav_id)}, (function(err, result) {
	    skug_nav_menu.find({},(function(err,  all_nav_menu) {
		    if(err) res.json(err);
		    res.render('backend/view-all-nav-menu.ejs', {all_nav_menu_data: all_nav_menu, active_user_details: active_user_details, page_url: page_url})
		}));
	}));	
}