var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});

var upload = multer({
 	storage: Storage
}).any();
// GET/User Login action from mongo.
var gallery_id;

	//@for add one more image page rendering on backend/add_more_images.
exports.add_more_image_ejs = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	res.render('backend/add_more_images', { active_user_details: active_user_details, page_url: page_url });
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	 gallery_id = qdata.id;
}
	//@for submit form with one file upload in a mongodb. 
exports.add_more_images_submit  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        	//@ this query for finding gallery image array with the help of gallery id and update .
	    skug_gallery.findOne({"_id" : ObjectId(gallery_id)},(function(err, result) {
		    if(err) res.json(err);
		    var gallery_object = result.gallery;
		    gallery_object.push(req.files[0].filename);
			skug_gallery.updateOne(
				{"_id":Object(gallery_id)} ,
					{
						$set: { 
							    gallery : gallery_object
					    }
					}, function(err, Gallery_image) {
						// @find all gallery data and send on a view all gallery page. 
				skug_gallery.find({},(function(err,  gallery_data) {
				//  	// If error arises, i.e. data not inserted
				     	if (err) throw err;
				//  	// else on succes
				 
			   		res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url});
			 	}));
		 	});
		}));
	});
}; // exports.login ends here.
