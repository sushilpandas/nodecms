var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
//console.log('storage ' + Storage);
var upload = multer({
     	storage: Storage
 	}).any();
//# Export for Delete all gallery data here.....
exports.delete_gallery_data = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var p_id = qdata.id
	var page_name = qdata.pname; 

//Delete Query for gallery data begins Here------------------>
	skug_gallery.deleteOne({"_id":ObjectId(p_id)}, (function(err, result) {
		skug_gallery.find({},(function(err, gallery_data) {
		    if(err) res.json(err);
		    res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url});
		}));
	}));
}; //# End Export Here.
