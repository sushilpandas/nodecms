var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_page  = mongoose.model('skug_page');
var skug_post  = mongoose.model('skug_post');
var banner_details  = mongoose.model('banner_details');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
    storage: Storage
}).any();
var banner_image;
var banner_id;
var new_image;
var del_banner_image;
var del_banner_id;
var add_banner_id;
exports.add_new_gallery  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
    res.render('backend/add_new_gallery', { active_user_details: active_user_details, page_url: page_url});
};

exports.submit_gallery  = function(req,res) {
	var fullUrl = req.originalUrl;
	if(req.body.Type === 'Page'){ 
	    var pagename = new Array();
		skug_page.find({},(function(err, pagedata) {
		    if(err) res.json(err);
			for(val in pagedata) {
				pagename.push(pagedata[val].pageName);
			}
			res.send({ type: req.body.Type, pagename: pagename });
		}));
	}
	else if(req.body.Type === 'Post'){ 
	    var postname = new Array();
		skug_post.find({},(function(err, postdata) {
		    if(err) res.json(err);
			for(val in postdata) {
				postname.push(postdata[val].postName);
			}
			res.send({ type: req.body.Type, postname: postname });
		}));
	}
	else{
	}
};
exports.submit_add_gallery = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	upload(req, res, function(err) {
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
		    var currentDate = new Date();
		skug_gallery.findOne({ ptype : req.body.ptype }, function(err, ptype){
		 	if( ptype  === null ){ 
				skug_gallery.insertMany(
				{
			    	title: req.body.title,
			    	type: req.body.Type,
			    	ptype: req.body.ptype,
					gallery: imgfile
				}, function(err, res) {
					if (err) throw err;
					console.log("Successfully Added")
			  	}); 
			  	skug_gallery.find({},(function(err,  gallery_data) {
			    	if (err) throw err;
			    	 res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url});
				}));
			}
			else{
				console.log("Allready Exist");
			}
		});
		skug_gallery.find({},(function(err,  gallery_data) {
	    	if(err) res.json(err);
	    	res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url});
		}));
	});
};


// @ --------------------------/  Start /------------------/ 
// @ Add Banner page render image controllers.
	exports.add_new_banner_image = function(req,res){
		var page_url = req.originalUrl;
		var active_user_details = req.session.user;
		res.render('backend/add_banner',{active_user_details: active_user_details, page_url: page_url});
	}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/ 
// @ Add Banner form action controllers.
	exports.add_banner_form_submit = function(req, res){
		var page_url = req.originalUrl;
		var active_user_details = req.session.user;
		upload(req, res, function(err) {
	        if (err) {
	         	console.log(err);
	            return res.end("Something went wrong!");
	        }
	        var imgfile  = [];
	        for(val in req.files) {
	        	imgfile.push(req.files[val].filename);
	        }
	        banner_details.findOne({ banner_title : req.body.banner_title }, function(err, banner_titles){
			 	if( banner_titles  === null ){
			        banner_details.insertMany(
					{
				    	banner_title: req.body.banner_title,
						banner_files: imgfile
					}, function(err, res) {
						if (err) throw err;
						console.log("Banner Successfully Added");
				  	});
		    	}
		    	else{
		    		console.log("Allready Exist");
		    	}
		    });
	    });
	    banner_details.find({},(function(err,  all_banner) {
	    	if(err) res.json(err);
	    	res.render('backend/view_all_banner', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url});
		}));
	}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/
// @ Manage all Banner.
exports.view_all_banners = function(req, res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	banner_details.find({},(function(err,  all_banner) {
    	if(err) res.json(err);
    	res.render('backend/view_all_banner', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url});
	}));
}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/
// @ Edit single Banner image.
exports.edit_banner_image = function(req, res){
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var active_user_details = req.session.user;
	var q = url_exp.parse(fullUrl, true);
	var page_url = req.originalUrl;
	var qdata = q.query; 
	banner_image = qdata.banner_img;
	banner_id = qdata.id;
	res.render('backend/edit_banner_image',{ page_url: page_url, active_user_details: active_user_details });
}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/
// @ edit Banner image submit action.
exports.edit_banner_image_submit = function(req, res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
		new_image = req.files[0].filename;
		banner_details.findOne({"_id" : ObjectId(banner_id)},(function(err, result) {
	    if(err) res.json(err);
	    var banner_object = result.banner_files;
	    var banner_value = banner_image;
	    var deleted_image = update_image(banner_object, banner_image);
		deleted_image.push(req.files[0].filename);
			banner_details.updateOne(
			{"_id":ObjectId(banner_id)} ,
				{
					$set: { 
						    banner_files : deleted_image 
				    }
			}, function(err, pageData_content) {
				banner_details.find({},(function(err,  all_banner) {
			    	if(err) res.json(err);
			    	res.render('backend/view_all_banner', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url});
				}));
		 	});
		}));
	});
}
// @---------------------------/  End ?--------------------/

// @ --------------------------/  Start /------------------/
// @ Delete single banner image.
exports.delete_banner_images = function(req,res){
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var active_user_details = req.session.user;
	var q = url_exp.parse(fullUrl, true);
	var page_url = req.originalUrl;
	var qdata = q.query; 
	banner_image = qdata.banner_img;
	banner_id = qdata.id;
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	banner_details.findOne({"_id" : ObjectId(banner_id)},(function(err, result) {
	    if(err) res.json(err);
	    console.log(result);
	    var banner_object = result.banner_files;
	    var banner_value = banner_image;
	    var deleted_image = update_image(banner_object, banner_image);
		banner_details.updateOne(
		{"_id":ObjectId(banner_id)} ,
			{
				$set: { 
					    banner_files : deleted_image 
			    }
		}, function(err, pageData_content) {
			banner_details.find({},(function(err,  all_banner) {
		    	if(err) res.json(err);
		    	res.render('backend/view_all_banner', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url});
			}));
	 	});
	}));
}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/
// @ add one more banner image.
exports.add_more_banner_images = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	res.render('backend/add_one_more_banner', { active_user_details: active_user_details, page_url: page_url });
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	add_banner_id = qdata.id;
}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/
// @ add one more banner
exports.add_one_more_banner_submit = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        	//@ this query for finding banner image array with the help of banner id and update .
	    banner_details.findOne({"_id" : ObjectId(add_banner_id)},(function(err, result) {
		    if(err) res.json(err);
		    var banner_array = result.banner_files;
		    banner_array.push(req.files[0].filename);
			banner_details.updateOne(
				{"_id":Object(add_banner_id)} ,
					{
						$set: { 
							    banner_files : banner_array
					    }
					}, function(err, Gallery_image) {
				banner_details.find({},(function(err,  all_banner) {
			    	if(err) res.json(err);
			    	res.render('backend/view_all_banner', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url});
				}));
		 	});
		}));
	});
}
// @---------------------------/  End ?--------------------/


// @ --------------------------/  Start /------------------/
// @ delete banner details.
exports.delete_banner_details = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var del_banner_id = qdata.id; 
	//Delete Query for gallery data begins Here------------------>
	banner_details.deleteOne({"_id":ObjectId(del_banner_id)}, (function(err, result) {
		banner_details.find({},(function(err,  all_banner) {
	    	if(err) res.json(err);
	    	res.render('backend/view_all_banner', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url});
		}));
	}));
}
// @---------------------------/  End ?--------------------/


	//Remove function is here for remove one banner image an array.  
function update_image(banner_object, banner_value) {
    var index = null;
    while ((index = banner_object.indexOf(banner_value)) !== -1)
        banner_object.splice(index, 1);
    return banner_object;
};