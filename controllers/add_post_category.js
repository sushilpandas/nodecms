var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post_category  = mongoose.model('skug_post_category');
var skug_post_seo  = mongoose.model('skug_post_seo');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
    //@for render page on backend/add-post-category. 
exports.add_new_post_category = function(req,res){
    var page_url = req.originalUrl;
    var active_user_details = req.session.user;
    res.render('backend/add-post-category', { active_user_details: active_user_details, page_url:page_url } );
};

    //@add new category on clicking on submit button. 
exports.add_new_post_category_submit = function(req,res){
    var active_user_details = req.session.user;
    skug_post_category.findOne({ category_name: req.body.category_name }, function(err, postData){
        if( postData === null ){
            skug_post_category.insertMany({
                category_name                : req.body.category_name,
                category_slug                : req.body.category_slug,
                category_description         : req.body.category_description,
                cat_seo_title                : req.body.cat_seo_title,
                cat_seo_descriptions         : req.body.cat_seo_description,
                cat_seo_keywords             : req.body.cat_seo_keywords,
                cat_seo_canonicals           : req.body.cat_seo_canonical,
                cat_og_title                 : req.body.cat_og_title,
                cat_og_descriptions          : req.body.cat_og_description,
                cat_og_urls                  : req.body.cat_og_url,
                cat_og_types                 : req.body.cat_og_type,
                cat_og_images                : req.body.cat_og_image,
                cat_og_site_names            : req.body.cat_og_site_name,
                cat_twitter_titles           : req.body.cat_twitter_title,
                cat_twitter_descriptions     : req.body.cat_twitter_description,
                cat_twitter_cards            : req.body.cat_twitter_card,
                cat_twitter_sites            : req.body.cat_twitter_site,
                cat_twitter_creators         : req.body.cat_twitter_creator
            },function(err, res) {
                // If error arises, i.e. data not inserted
                if (err) throw err;
                //insert Query for  SKUG_SEO_POST------------------->
                console.log("succesfull");
                // });
            }); // Insert query ends here.
        }
        else{
            console.log("This post Name Allready exist please select unother One Thank you");
        }
    });
    skug_post_category.find({},(function(err, post_category) {
        var page_url = req.originalUrl;
            var active_user_details = req.session.user;
        if(err) res.json(err);
            res.render('backend/view-all-post-category', {post_category: post_category, active_user_details: active_user_details, page_url: page_url});
    }));
}; //@ Export End Here.