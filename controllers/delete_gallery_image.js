var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
// GET/User Login action from mongo.
var gallery_image;
var gallery_id;

//# Export For Delete one Gallery image at time.
exports.delete_gallery_images  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	 gallery_image = qdata.img_url;
	 gallery_id = qdata.id;
//# Find Query for checking the image id.
    skug_gallery.findOne({"_id" : ObjectId(gallery_id)},(function(err, result) {
	    if(err) res.json(err);
		    var gallery_object = result.gallery;
		    var gallery_value = gallery_image;
		    var deleted_image = remove_image(gallery_object, gallery_image);
		skug_gallery.updateOne({"_id":ObjectId(gallery_id)} ,{$set:{ gallery : deleted_image }}, function(err, pageData_content) {
				//# Find Query for find all the Data after delete image.
			skug_gallery.find({},(function(err,  gallery_data) {
		    	// If error arises, i.e. data not inserted
		    	if (err) throw err;
		    	// else on succes
		    	res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url}); //# Redirect on view all gallery pages.
	    	}));
	  	});
	}));
}; // exports End.

//# remove operation for delete image here 
function remove_image(gallery_object, gallery_value) {
    var index = null;
    while ((index = gallery_object.indexOf(gallery_value)) !== -1)
        gallery_object.splice(index, 1);
    return gallery_object;
};