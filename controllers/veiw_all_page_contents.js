var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_page  = mongoose.model('skug_page');
var skug_page_seo  = mongoose.model('skug_page_seo');
var skug_nav_menu  = mongoose.model('skug_nav_menu');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
var url = "mongodb://localhost:27017/";
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
     	storage: Storage
 	}).any();
// exports.pageData_contents  = function(req,res) {
// 	var page_url = req.originalUrl;
// 	var active_user_details = req.session.user;
// 		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
// 		var q = url_exp.parse(fullUrl, true);
// 		var qdata = q.query; 
// 		var page_url = qdata.page_url; 
// 	skug_page.find({URL : page_url},(function(err, result) {
// 	    if(err) res.json(err);
// 	    // console.log(result);
// 		skug_page.find({},(function(err, page_content) {
// 		    if(err) res.json(err);
// 		    // console.log(result);
// 		    res.render('backend/pageData-contents', {pagedata: result, pagename: page_content, active_user_details: active_user_details, page_url: page_url});
// 		}));
// 	}));
// };
	//@ find all page Data On click on submit Button and render on backend/view_all_pages.
exports.view_all_page_data = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	skug_page.find({},(function(err,  pagedata) {
	    if(err) res.json(err);
	    res.render('backend/view_all_pages', {pageData_content: pagedata, active_user_details: active_user_details, page_url: page_url});
	}));
}
	//@ Home pageexports here. 
// exports.index_page_info  = function(req,res) {
// 	var page_url = req.originalUrl;
// 	var active_user_details = req.session.user;
// 		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
// 		var q = url_exp.parse(fullUrl, true);
// 		var qdata = q.query; 
// 		var page_url = qdata.page_url; 
// 	skug_page.find({URL : page_url},(function(err, result) {
// 	    if(err) res.json(err);
// 		skug_page.find({},(function(err, page_content) {
// 		    if(err) res.json(err);
// 			    skug_nav_menu.find({},(function(err, all_nav_menu) {
// 			    res.render('frontend/page-info', {pagedata: result, all_nav_menu_data: all_nav_menu, active_user_details: active_user_details, page_url: page_url});
// 			}));
// 		}));
// 	}));
// };