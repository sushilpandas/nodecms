// var http        = require("http");
var express     = require("express");
var app         = express();
var path        = require("path");
var mongoose        = require('mongoose');
var fs          = require('fs');
var url_exp         = require('url');
var skug_page  = mongoose.model('skug_page');
var skug_post  = mongoose.model('skug_post');
var config            =   require('../configuration/config');
var skug_page_seo  = mongoose.model('skug_page_seo');
var skug_nav_menu  = mongoose.model('skug_nav_menu');
var model           = require('../models/model');
var skug_site_settings  = mongoose.model('skug_site_settings');
var banner_details  = mongoose.model('banner_details');
var skug_nav_menu  = mongoose.model('skug_nav_menu');
	//====================================================================================================//
    //                              .... Variable Declaration code goes here ...                          //
    //====================================================================================================//
    var dispaly_id;
	var banner_data;
	var templet_name;
// @ --------------------------/  Start /------------------/ 
	// @ exports for frontend theme render dynamically
	exports.theme_design_call = function(req, res){
		console.log("theme_design_call");
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		var q = url_exp.parse(fullUrl, true);
		var qdata = q.query; 
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		var q = url_exp.parse(fullUrl, true);
		var qdata = q.query; 
		getSiteSettingsDetails((site_settings) => {
			var home_page = "Home";
			skug_post.find().limit(5).sort({x:-1}).then(function(post_details) {
				skug_page.findOne({ pageName: home_page}, function(err, page_details){
					console.log(page_details);
					// banner_data = page_details.banner_page;
					templet_name = page_details.page_template;
				  var themes;
					for(val in site_settings){
						themes = site_settings[val].theam_name;
					}
					var menu_name = "main menu"
					skug_page_seo.findOne({ page_name: home_page}, function(err, seo_data){
						skug_nav_menu.findOne({ nav_title: menu_name}, function(err, menu_item){
							banner_details.findOne({ banner_title: banner_data}, function(err, banners){
								// console.log(banners);
								if (err) throw err;
							    res.render('frontend/theme/'+ themes +'/'+templet_name, { site_settings: site_settings, page_details: page_details, banners: banners, menu_item: menu_item, seo_data : seo_data, post_details: post_details});
							});
						});
					});
				});
			});
		});
	};

	exports.dynamic_page_call = function(req, res){
		dispaly_id = req.params.dispaly_id;
		// console.log(req.query);
		// console.log(req.query.status);
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		var q = url_exp.parse(fullUrl, true);
		var qdata = q.query; 
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		var q = url_exp.parse(fullUrl, true);
		var qdata = q.query; 
		getSiteSettingsDetails((site_settings) => {
			skug_post.find().limit(5).sort({x:-1}).then(function(post_details) {
				// console.log(post_details);
				skug_page.findOne({ URL: dispaly_id }, function(err, page_details){
					if (err) throw err;
					banner_data = page_details.banner_page;
					// console.log(banner_data);
					templet_name = page_details.page_template;
				  	var themes;
				 //  	// console.log(page_details.publish);
				  	for(val in site_settings){
						themes = site_settings[val].theam_name;
					}
					var menu_name = "main menu"
						skug_page_seo.findOne({ page_name: page_details.pageName}, function(err, seo_data){
							skug_nav_menu.findOne({ nav_title: menu_name}, function(err, menu_item){
								// console.log("menu_item");
								// console.log(menu_item);
								banner_details.findOne({ banner_title: banner_data}, function(err, banners){
									if (err) throw err;
										if( fullUrl.includes("?status") == true ){
									    res.render('frontend/theme/'+ themes +'/'+templet_name, { site_settings: site_settings, page_details: page_details, banners: banners, menu_item: menu_item, seo_data : seo_data, post_details: post_details});
									}
									else{
										if(page_details.publish == "Publish"){
											res.render('frontend/theme/'+ themes +'/'+templet_name, { site_settings: site_settings, page_details: page_details, banners: banners, menu_item: menu_item, seo_data : seo_data, post_details: post_details});
										}
										else{
											res.render('frontend/404');
										}
									}
								});
							});
						});
					});
				});	
			});
	};
// @---------------------------/  End ?--------------------/

/////////////////////////////////////////////////////////////////
///                       	functions						 ///
///////////////////////////////////////////////////////////////

// @------------------------------------------------// Start -------//
// callback fucton for fetch site settings details.
	function getSiteSettingsDetails(callback) {
		skug_site_settings.find({},(function(err,  site_settings) {
		    if(err)
		        callback(err)
		    else 
		        callback(site_settings); 
		}));
	};
// @----------------------------------------------------// End -----//