var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post  = mongoose.model('skug_post');
var skug_post_category  = mongoose.model('skug_post_category');
var skug_post_seo  = mongoose.model('skug_post_seo');
var skug_category_relation  = mongoose.model('skug_category_relation');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating image storage folder for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
//# Creating a variable for to send image one or Multiple. image folder and Mongo.
var upload = multer({
     	storage: Storage
 	}).any();
// Export For Page Redirect On add new post page.
exports.add_new_postPage = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	skug_post_category.find({},(function(err, category_data) {
	    if(err) res.json(err);
	    res.render('backend/add_post_page', {category_name: category_data, active_user_details: active_user_details, page_url: page_url});
	}));
};

//@ On GET/Submit action - insert Post data Here in Mongo database.
exports.add_post_page = function(req,res){
	var active_user_details = req.session.user;
		//# image uploade operation begins Here....
 	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
		    var currentDate = new Date();//# for current Date.
		    // console.log(currentDate);
		    // Create a mongo query with all form parameters to be inserted
			 //    autoIncrement.getNextSequence(dbo, "skug_post", function (err, autoIndex) {
			// var collection = dbo.collection("skug_post");
			//# Find Query For Check Rdundent data Here or Not in mongodb.
		skug_post.findOne({ postName: req.body.postName }, function(err, postData){
			if( postData === null ){ //# if postData is not exist then success.
					//# Insert Query for post data send in Mongo db. here.
				skug_post.insertMany({
					postName: req.body.postName, 
					// id:autoIndex,
					post_url: req.body.post_url,
					featured_imaeg: req.files[0].filename,
					contents: req.body.content, 
					post_description: req.body.post_description,
					gallery: imgfile,
					publish : req.body.publish,
					createDate: currentDate
				},function(err, res) {
			    	// If error arises, i.e. data not inserted
			    	if (err) throw err;
					//insert Query for  SKUG_SEO_POST------------------->
			   skug_post_seo.insertMany({
			   		// aouto_Id:autoIndex,
					// post_id:post_id,
					post_name: req.body.postName,
					seo_title: req.body.seo_title,
					seo_descriptions: req.body.seo_description,
					seo_keywords: req.body.seo_keywords,
					seo_canonicals: req.body.seo_canonical,
					og_title: req.body.og_title,
					og_descriptions: req.body.og_description,
					og_urls: req.body.og_url,
					og_types: req.body.og_type,
					og_images: req.body.og_image,
					og_site_names: req.body.og_site_name,
					twitter_titles: req.body.twitter_title,
					twitter_descriptions: req.body.twitter_description,
					twitter_cards: req.body.twitter_card,
					twitter_sites: req.body.twitter_site,
					twitter_creators: req.body.twitter_creator
				}, function(err, res) {
			    	if (err) throw err;
			    	console.log("1 document inserted");
			  	});
				   for(val_categories in req.body.category){
				   	skug_category_relation.insertMany({
					postName: req.body.postName, 
					post_url: req.body.post_url,
					post_category: req.body.category[val_categories],
					post_featured_imaeg: req.body.featured_image
						
					}, function(err, res) {
				    	if (err) throw err;
				    	console.log("category Relation data inserted document inserted");
				  	});
				}
			  	}); // Insert query ends here.
			}
			else{
			  	console.log("This post Name Already exist please select unother One Thank you");
			}
		});
	});	//Image Upload function END Here------------------->
	// Find Query for find allpost data send view all post data page.------------>
		skug_post.find({},(function(err, postdata) {
			var page_url = req.originalUrl;
		    if(err) res.json(err);
		    res.render('backend/view_all_post', {post_page: postdata, active_user_details: active_user_details, page_url: page_url});
		}));
};//Export END Here ----------->
