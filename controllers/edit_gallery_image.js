var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
     	storage: Storage
 	}).any();
var gallery_image;
var gallery_id;
var new_image;

	//@ for render on a edit gallery image page. 
exports.edit_gallery_images_img = function(req,res){
	var active_user_details = req.session.user;
	var page_url = req.originalUrl;
	console.log("I reached Here");
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	gallery_image = qdata.img_url;
	gallery_id = qdata.id;
	// console.log(page_url);
	res.render('backend/edit_gallery_img',{ page_url: page_url, active_user_details: active_user_details });
	
}
	//@ for Edit gallery contents.  
exports.edit_gallery_images  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
		new_image = req.files[0].filename;
		skug_gallery.findOne({"_id" : ObjectId(gallery_id)},(function(err, result) {
	    if(err) res.json(err);
	    var gallery_object = result.gallery;
	    var gallery_value = gallery_image;
	    var deleted_image = remove_image(gallery_object, gallery_image);
		deleted_image.push(req.files[0].filename);
			skug_gallery.updateOne(
			{"_id":ObjectId(gallery_id)} ,
				{
					$set: { 
						    gallery : deleted_image 
				    }
			}, function(err, pageData_content) {
				skug_gallery.find({},(function(err,  gallery_data) {
				 	// If error arises, i.e. data not inserted
				    	if (err) throw err;
			  		res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url});
			  	}));
		 	});
		}));
	});
}; 
	//Remove function is here for remove one gallery image delete in an array here.  
function remove_image(gallery_object, gallery_value) {
    var index = null;
    while ((index = gallery_object.indexOf(gallery_value)) !== -1)
        gallery_object.splice(index, 1);
    return gallery_object;
};