var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post  = mongoose.model('skug_post');
var skug_post_seo  = mongoose.model('skug_post_seo');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
     	storage: Storage
}).any();

//@ Export for Delete Post data------------------------>
exports.delete_post_page = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
		//# Find Post Id and Post name Here...
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var p_id = qdata.pid
	var page_name = qdata.pname; 
	//Delete post page Query begins Here------------------>
	skug_post.deleteOne({"_id":ObjectId(p_id)}, (function(err, result) {
	    if(err) res.json(err);
	    console.log("skug_post One Document Deleted Successfully")
	}));
	//Delete Post_SEO page Begins here------------------------------------------>
	skug_post_seo.deleteOne({"post_id":ObjectId(p_id)}, (function(err, result_seo) {
	    if(err) res.json(err);
	}));//Delete Post_SEO END here------------------------------------------>
	//Find post Query begins Here------------------>
    skug_post.find({},(function(err, postdata) {
	    if(err) res.json(err);
	    res.render('backend/view_all_post', {post_page: postdata, active_user_details: active_user_details, page_url: page_url});
	}));
};
