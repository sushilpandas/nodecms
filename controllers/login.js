var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
const nodemailer           = require('nodemailer');
var url_exp         = require('url');
var mongoose        = require('mongoose');
// User Schema declation here.
var registration  = mongoose.model('registration');
var config            =   require('../configuration/config');
var skug_users  = mongoose.model('skug_user');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");


    // GET/User Login action from mongo.
    exports.log_in_page  = function(req,res) {
      res.render('backend/login');
    };

    // --------------------------------/satrt------>
    // admin login and session create here.
    exports.login  = function(req,res) {
      // console.log("login submit");
      if(!req.body.email && !req.body.password) {
          res.json({status: false, error: "Enter Required Fields!"});
      } else {
          skug_users.findOne({ email: req.body.email, password: req.body.password })
          .then(user => {
              if(!user) {
                  res.json({status: false, error: "Wrong Credentials!"});
              } else {
                req.session.user = user;
                req.session.isLoggedIn = true;
                return req.session.save(err => {
                    console.log(err);
                    // console.log(err);
                    // console.log(user);
                    res.json({status: true, user: user});
                    // res.render('/backend/admin-dashboard')
                })
              }
          })
          .catch(error => {
              console.log(error);
              res.json({status: false, error: "Incorrect login credentials!"});
          })
      } 
    }; // exports.login ends here.
    // --------------------------------/end------>

    // --------------------------------/satrt------>
    // admin log out and session destroy.
    exports.log_out  = function(req,res) {
        req.session.destroy(error => {
            console.log(error);
            res.redirect('/backend');
        });
    };
    // --------------------------------/end------>

    // --------------------------------/satrt------> 
    // forget admin login password 
    exports.forget_user_password = function(req,res){
      skug_users.findOne({ email: req.body.femail}, function(err, user_details) {
          // console.log(user_details);
          const output ='<p>You have a new forget user password request!<h3>User Details<h3>'
        + '<ul>' +
            '<li> Name: '+ user_details.first_name + '</li>'+
            '<li> Email: '+ user_details.email +'</li>'+
            '<li> Password: '+ user_details.password +'</li>'+
            '<li> Phone Number: '+ user_details.mobile +'</li>'+
            '<li> Role Type: '+ user_details.user_role +'</li>'+
        '</ul>';

        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        // let account = await nodemailer.createTestAccount();
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: "smtp.hostinger.in",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'tripinn@websitepandas.com', // generated ethereal user
                pass: 'ca1158ca' // generated ethereal password
            },
            tls:{
                rejectUnauthorized:false
            }
        });
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Admin Login page query Form 👻" <tripinn@websitepandas.com>', // sender address
            //to: "support@tripinn.com", // list of receivers
             to: "ranjan@skuginsights.com", // list of receivers
            subject: "SkugInsights | Query for forget user password", // Subject line
            text: "Queries from user", // plain text body
            html: output // html body
        };
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if(error) {
                return console.log(error);
            }
            res.redirect('/backend');
        });
      });
    }
    // --------------------------------/end------>
  