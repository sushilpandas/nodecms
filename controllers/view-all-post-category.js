var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post_category  = mongoose.model('skug_post_category');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");

	//@	View all post data here------------------------------>
exports.view_all_post_category_here = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	skug_post_category.find({},(function(err, post_category) {
	    if(err) res.json(err);
	    	res.render('backend/view-all-post-category', {post_category: post_category, active_user_details: active_user_details, page_url: page_url});
	}));
}