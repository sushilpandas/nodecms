var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_post_category  = mongoose.model('skug_post_category');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
     	storage: Storage
}).any();

//@ Export for Delete category------------------------>
exports.delete_post_category = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var c_id = qdata.cid
	var cate_name = qdata.cname; 
		//@ delete mongo query is here.
	skug_post_category.deleteOne({"_id":ObjectId(c_id)}, (function(err, result) {
	    if(err) res.json(err);
	    console.log("skug_post One Document Deleted Successfully")
	}));
		//@ Mongo Find Query here .
    skug_post_category.find({},(function(err, post_category) {
	    if(err) res.json(err);
	    res.render('backend/view-all-post-category', {post_category: post_category, active_user_details: active_user_details, page_url: page_url});
	}));
};
