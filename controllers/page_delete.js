var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_page  = mongoose.model('skug_page');
var skug_page_seo  = mongoose.model('skug_page_seo');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
	//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
     	storage: Storage
 	}).any();
	// @ page delete export here.
exports.delete_page = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var p_id = qdata.pid
	var page_name = qdata.pname; 

		//@ Delete all page data when click delete button. 
	skug_page.deleteOne({"_id":ObjectId(p_id)}, (function(err, result) {
	    if(err) res.json(err);
	}));

		// @ Delete all seo data when page name delete.
	skug_page_seo.deleteOne({"page_id":ObjectId(p_id)}, (function(err, result_seo) {
	    if(err) res.json(err);
	}));

		// @ find all all page data and send on backend/view_all_pages.
	skug_page.find({},(function(err, pagedata) {
	    if(err) res.json(err);
	    res.render('backend/view_all_pages', {pageData_content: pagedata, active_user_details: active_user_details, page_url: page_url});
	}));
};