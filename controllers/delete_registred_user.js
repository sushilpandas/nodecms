var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_user  = mongoose.model('skug_user');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
var upload = multer({
     	storage: Storage
 	}).any();
//#Export for Delete Register user. Here..
exports.delete_register_user = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
		//# find id and user id for delete user data here.
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var q = url_exp.parse(fullUrl, true);
	var qdata = q.query; 
	var user_id = qdata.userid;
	skug_user.deleteOne({"_id":ObjectId(user_id)}, (function(err, result) {
	    if(err) res.json(err);
	    console.log("One Document Deleted Successfully")
	}));
		//@ mongo Find Query here.
	skug_user.find({},(function(err, user_data) {
	    if(err) res.json(err);
	    res.render('backend/view-all-registered-user', {all_user: user_data, active_user_details: active_user_details, page_url: page_url});
	}));
};//#End Export Here...