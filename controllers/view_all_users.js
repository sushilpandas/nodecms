var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_user  = mongoose.model('skug_user');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");

	// @ find all registered user and send on backend/view-all-registered-user.ejs.
exports.view_all_registered_users = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
    skug_user.find({},(function(err,  all_user_data) {
	    if(err) res.json(err);
	    res.render('backend/view-all-registered-user.ejs', {all_user: all_user_data, active_user_details: active_user_details, page_url: page_url});
	}));
}