var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_user  = mongoose.model('skug_user');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
//console.log('storage ' + Storage);
var upload = multer({
    storage: Storage
}).any();
	// @ Render on a update all user data here.
exports.edit_user_details = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
  	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  	var q = url_exp.parse(fullUrl, true);
  	var qdata = q.query; 
  	console.log(qdata.userid);
  	var user_id = qdata.userid;
  	var page_name = qdata.pname; 
	skug_user.findOne({"_id":ObjectId(user_id)},(function(err, user_data) {
		console.log(user_data);
    if(err) res.json(err);
	    res.render('backend/edit_user_details.ejs', {all_user: user_data, active_user_details: active_user_details, page_url: page_url});
	}));
};
	// @update all user data on clicking on a submit button here.
exports.edit_user_all_data   = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	//@ Upload image begins here...
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
        var updateTime = new Date();
	    console.log(imgfile);
		if(imgfile != null){
	  	skug_user.updateOne({"_id":ObjectId(req.body.id)} ,{$set: 
	  		{
		  		first_name: req.body.first_name,
				last_name : req.body.last_name,
				user_id : req.body.user_id,
				password : req.body.password,
				email : req.body.email, 
				mobile : req.body.mobile,
				user_role : req.body.user_role,
				image : req.files[0].filename 
			}
		}, function(err, res) {
	    	// If error arises, i.e. data not inserted
	    	if (err) throw err;
	    	// else on succes
	    	console.log("1 document Update");
	  	}); // Updates query ends here.
	  	}
		else{
		  	skug_user.updateOne({"_id":ObjectId(req.body.id)} ,{$set:
			  	{
				  	first_name: req.body.first_name,
					last_name: req.body.last_name,
					user_id: req.body.user_id,
					// password: req.body.password,
					email: req.body.email, 
					mobile: req.body.mobile,
					user_role:req.body.user_role
				}
			}, function(err, res) {
		    	// If error arises, i.e. data not inserted
		    	if (err) throw err;
		    	// else on succes
		    	console.log("1 document Update");
		  	}); //skug_page Update query ends here.
		}
	}); 
	skug_user.find({},(function(err, all_user_data) {
	    if(err) res.json(err);
		res.render('backend/view-all-registered-user.ejs', {all_user: all_user_data, active_user_details: active_user_details, page_url: page_url});
	}));
};