var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_user  = mongoose.model('skug_user');
var url_exp         = require('url');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage for the image
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
//create variable upload for file destination.
var upload = multer({
     	storage: Storage
}).any();
exports.user_registration_here = function(req, res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	res.render('backend/user-register', { active_user_details: active_user_details, page_url: page_url });
}
exports.user_registration  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	//@ Upload image begins here...
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
		// Insert form parameters in the collection "skug_user" using above query named "user"
		skug_user.findOne({ user_id: req.body.user_id }, function(err, users_id){
			if( users_id === null ){
			  skug_user.insertMany({
			  	first_name: req.body.first_name,
				last_name: req.body.last_name,
				user_id: req.body.user_id,
				password: req.body.password,
				email: req.body.email, 
				mobile: req.body.mobile,
				user_role:req.body.user_role,
				image: req.files[0].filename 
			}, function(err, res) {
			    	// If error arises, i.e. data not inserted
			    	if (err) throw err;
			    	// else on succes
			    	console.log("New User register Successfully ");
			  	}); // Insert query ends here.
			}
			else{
			  	console.log("User  Allready exist please Enter unother One Thank you");
			}
		});
	});// Upload image begins here...
skug_user.find({},(function(err,  all_user_data) {
	    if(err) res.json(err);
	    res.render('backend/view-all-registered-user.ejs', {all_user: all_user_data, active_user_details: active_user_details, page_url: page_url});
		}));
}; // exports.submit ends here.







