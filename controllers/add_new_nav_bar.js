var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_page  = mongoose.model('skug_page');
var skug_post  = mongoose.model('skug_post');
var skug_post_category  = mongoose.model('skug_post_category');
var custom_menues  = mongoose.model('custom_menues');
var skug_category_relation  = mongoose.model('skug_category_relation');
var skug_nav_menu  = mongoose.model('skug_nav_menu');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating storage destination for the image.....
    //@find all category,all pages,all post and all gallery name. And then render backend/add-nav-bar page.
exports.nav_bar_page  = function(req,res) {
    var page_url = req.originalUrl;
    var active_user_details = req.session.user;
	skug_post_category.find({},(function(err,  postcategory) {
        skug_page.find({},(function(err,  pagedata) {
            skug_post.find({},(function(err,  postdata) {
                skug_gallery.find({},(function(err,  gallery) {
                    if(err) res.json(err);
                    custom_menues.find({},(function(err,  all_custom_nav_menu) {
                        if(err) res.json(err);
                        res.render('backend/add-nav-bar',{postcategory_data: postcategory, pageData_content: pagedata, postalldata: postdata ,gallery_name: gallery,all_custom_menu_data: all_custom_nav_menu, active_user_details: active_user_details, page_url: page_url});
                    }));
                }));
            }));    
        }));    
	}));
};// exports.submit ends here.

//Add navigation bar here on submit------------------>
exports.add_nav_bar_submit = function(req,res){
    console.log("add_nav_bar_submit");
    // console.log(req.body.page_slug);
    var page_url = req.originalUrl;
    var active_user_details = req.session.user;
    skug_nav_menu.insertMany(
        {
            nav_title            : req.body.nav_bar_title,
            custom_menu          : req.body.custom_menu,
            custom_nav_menu_link : req.body.custom_menu_links,
            post_category        : req.body.post_bar_category,
            // category_slug        : req.body.category_url,
            page_name            : req.body.menu_item,
            page_slug            : req.body.menu_item_slug,
            post_name            : req.body.post_bar_name,
            gallery_title        : req.body.gallery_bar_title
        },function(err, res) {
            // If error arises, i.e. data not inserted
        if (err) throw err;
        console.log("succesfull");
    });
    skug_nav_menu.find({},(function(err,  all_nav_menu) {
        if(err) res.json(err);
        // console.log(result);
        res.render('backend/view-all-nav-menu.ejs', {all_nav_menu_data: all_nav_menu, active_user_details: active_user_details, page_url: page_url});
    }));
}; //@ Export End Here.

//Add custom navigation bar here on submit------------------>
exports.custom_nav_bar = function(req,res){
    var page_url = req.originalUrl;
    var active_user_details = req.session.user;
    custom_menues.insertMany(
        {
          custom_menu       : req.body.custom_menu,
          custom_menu_link  : req.body.custom_menu_link
        },function(err, res) {
            // If error arises, i.e. data not inserted
        if (err) throw err;
        console.log("add succesfull custom_menues ");
    });
    custom_menues.find({},(function(err,  all_custom_menu) {
        if(err) res.json(err);
        skug_post_category.find({},(function(err,  postcategory) {
            skug_page.find({},(function(err,  pagedata) {
                skug_post.find({},(function(err,  postdata) {
                    skug_gallery.find({},(function(err,  gallery) {
                        if(err) res.json(err);
                        res.render('backend/add-nav-bar',{postcategory_data: postcategory, pageData_content: pagedata, postalldata: postdata ,gallery_name: gallery,all_custom_menu_data: all_custom_menu, active_user_details: active_user_details, page_url: page_url});
                    }));
                }));    
            }));    
        }));
    }));
};