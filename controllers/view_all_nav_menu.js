var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_nav_menu  = mongoose.model('skug_nav_menu');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");

	// @ find all nav menues and send on backend/view-all-nav-menu.ejs.
exports.view_all_nav_menu = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
    skug_nav_menu.find({},(function(err,  all_nav_menu) {
	    if(err) res.json(err);
	    res.render('backend/view-all-nav-menu.ejs', {all_nav_menu_data: all_nav_menu, active_user_details: active_user_details, page_url: page_url});
	}));
}