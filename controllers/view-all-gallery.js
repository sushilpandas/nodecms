var express     = require("express");
var app         = express();
var path        = require("path");
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var url_exp         = require('url');
var mongoose        = require('mongoose');
var model           = require('../models/model');
var skug_gallery  = mongoose.model('skug_gallery');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");

	// @ find all gallery data and send on backend/view-all-gallery
exports.view_all_gallery = function(req,res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	skug_gallery.find({},(function(err,  gallery_data) {
	    if(err) res.json(err);
	    console.log("All gallery images");
	    console.log(gallery_data);
	    res.render('backend/view-all-gallery', {gallery_contents: gallery_data, active_user_details: active_user_details, page_url: page_url});
	}));
}