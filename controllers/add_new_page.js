var express     = require("express");
var app         = express();
var path        = require("path");
var mongoose        = require('mongoose');
var mysql       = require('mysql');
var multer		= require('multer');
var fs          = require('fs');
var model           = require('../models/model');
var skug_page  = mongoose.model('skug_page');
var skug_page_seo  = mongoose.model('skug_page_seo');
var banner_details  = mongoose.model('banner_details');
var skug_site_settings  = mongoose.model('skug_site_settings');
var url_exp         = require('url');
var ObjectId = require('mongodb').ObjectId;
var autoIncrement = require("mongodb-autoincrement");
//@ Creating image storage folder for the image.....
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/backend/image");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
	//# Creating a variable for to send image one or Multiple. image folder and Mongo.
var upload = multer({
     	storage: Storage
}).any();
	// .array("imgUploader", 100);
	//# Export for redirect add page ....
// exports.data_insertForm  = function(req,res){
// 	var page_url = req.originalUrl;
// 	var page_url = req.originalUrl;
// 	var active_user_details = req.session.user;
// 	console.log("call 01");
// res.render('/backend/data_insertForm', { active_user_details: active_user_details, page_url: page_url });
// };
exports.add_new_pages  = function(req,res) {
	var all_templets = [];
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	getSiteSettingsDetails((site_settings) => {
		for(val in site_settings){
			var theme_name = site_settings[val].theam_name;
		}
		// console.log(theme_name);
		var dir = './public/frontend/theme/'+ theme_name;
		fs.readdir(dir, function (err, all_templet) {
		    //handling error
		    if (err) {
		        return console.log('Unable to scan directory: ' + err);
		    }
		    for(value in all_templet ){
		    	var arr = all_templet[value];
		    	if(arr.includes(".ejs") == true){
		    		all_templets.push(arr);
		    	}
		    }
			banner_details.find({},(function(err,  all_banner) {
		    	if(err) res.json(err);
		    	res.render('backend/add_new_page', {all_banner: all_banner, active_user_details: active_user_details, page_url: page_url, all_templets: all_templets});
			}));
		});
	});
};
//@ On GET/Submit action - insert Page data Here in Mongo database
exports.submit_dataInsert  = function(req,res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	//@ Upload image begins here...
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
	    var currentDate = new Date();
	    //Create aout increament id in mogodb.
		// autoIncrement.getNextSequence(dbo, "skug_page", function (err, autoIndex) {
		// var collection = dbo.collection("skug_page");
			//# Find Query For Check Rdundent data Here or Not.
		skug_page.findOne({ pageName: req.body.pageName }, function(err, pageData){
			if( pageData ===null ){ //# if page exist then success.
				//# Insert Query for send page data in mongodb.
				skug_page.insertMany(
				{
					pageName: req.body.pageName, 
					// id:autoIndex,
					URL: req.body.pageUrl,
					featuredImaeg: req.files[0].filename,
					contents: req.body.content, 
					message: req.body.message,
					gallery: imgfile,
					banner_page : req.body.banner_page,
					page_template: req.body.page_template,
					publish: req.body.publish,
					CreateDate: currentDate
				}, function(err, res) {
			    	// If error arises, i.e. data not inserted
			    	if (err) throw err;
			    	// else on succes
			    	//SEO Page Meta Data Enter Here Operation-------------->
				    // var page_id = res.ops[0]._id;    //get id from skug_page mongodb.
				    // var page_name_seo = res.ops[0].pageName;
			    	// autoIncrement.getNextSequence(dbo, "skug_page_seo", function (err, seoautoIndex) {
	    			// var collection = dbo.collection("skug_page_seo");
	    			// console.log("to be updated: " + page_id);
					skug_page_seo.insertMany(
					{
						// aouto_Id:seoautoIndex,
						// page_id:page_id,
						// page_name:page_name_seo,
						page_name: req.body.pageName,
						seo_title: req.body.seo_title,
						seo_descriptions: req.body.seo_description,
						seo_keywords: req.body.seo_keywords,
						seo_canonicals: req.body.seo_canonical,
						og_title: req.body.og_title,
						og_descriptions: req.body.og_description,
						og_urls: req.body.og_url,
						og_types: req.body.og_type,
						og_images: req.body.og_image,
						og_site_names: req.body.og_site_name,
						twitter_titles: req.body.twitter_title,
						twitter_descriptions: req.body.twitter_description,
						twitter_cards: req.body.twitter_card,
						twitter_sites: req.body.twitter_site,
						twitter_creators: req.body.twitter_creator
					}, function(err, res) {
				    	if (err) throw err;
				  	});
				  	   //});
			  	}); // Insert query ends here.
				//End SEO Meta Here---------------------------------->
			}
				else{
			  	console.log("This page Name Allready exist please select unother One Thank you");
			}
		});
	}); // Upload image End here...
    skug_page.find({},(function(err, pagedata) {
	    if(err) res.json(err);
		 res.render('backend/view_all_pages', {pageData_content: pagedata, active_user_details: active_user_details, page_url: page_url});
	}));
}; // exports.submit ends here.
// @----------------------// Start -------//
// @ get file(image) on ckeditor 
exports.getFile = function(req, res){
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	const images = fs.readdirSync('public/backend/image')
    var sorted = []
    for (let item of images){
        if(item.split('.').pop() === 'png'
        || item.split('.').pop() === 'jpg'
        || item.split('.').pop() === 'jpeg'
        || item.split('.').pop() === 'gif'
        || item.split('.').pop() === 'mp4'
        || item.split('.').pop() === 'svg'){
            var abc = {
                  "image" : "/backend/image/"+item,
                  "folder" : '/'
            }
            sorted.push(abc)
        }
    }
    // console.log(sorted);
    res.send(sorted);
};
// @------------------------// End -----//

// @----------------------// Start -------//
// @ upload file on cling browse button on ckeditor.
exports.upload =   function (req, res) {
	var page_url = req.originalUrl;
	var active_user_details = req.session.user;
	upload(req, res, function(err) {
		// If error arises
        if (err) {
         	console.log(err);
            return res.end("Something went wrong!");
        }
        var imgfile  = [];
        for(val in req.files) {
        	imgfile.push(req.files[val].filename);
        }
		res.redirect('back');
	});
}
// @------------------------// End -----//

// @----------------------// Start -------//
// @ Delete uploaded file on cling cross button on ckeditor.
exports.delete_file = function(req,res){
  	var url_del = 'public' + req.body.url_del
  	if(fs.existsSync(url_del)){
  		fs.unlinkSync(url_del)
  	}
  	res.redirect('back')
}
// @------------------------// End -----//

// @---------------------------------------------------------// Start -------//
	// Site sittings conroller for add new site settings page call.
	exports.add_site_settings = function(req, res){
		var page_url = req.originalUrl;
		var active_user_details = req.session.user;
		var dir = "./public/frontend/theme";
		var themes = fs.readdirSync(dir);
		getSiteSettingsDetails((site_settings) => {
			for(val in site_settings){
				var selected_theam_name = site_settings[val].theam_name;
			}
			const all_themes = themes.indexOf(selected_theam_name);
			if (all_themes > -1) {
			  themes.splice(all_themes, 1);
			}
			console.log(themes);
            res.render('backend/add_site_settings',  {   
                site_settings: site_settings,
                active_user_details: active_user_details,
                page_url : page_url,
                themes : themes
            });
        });	
	}
// @--------------------------------------------------------------// End -----//

// @------------------------------------------------// Start -------//
// controllers for add new site settings on submit
	exports.add_new_site_settings = function(req,res){
	    var page_url = req.originalUrl;
	    var active_user_details = req.session.user;
	    upload(req, res, function(err) {
		// If error arises
	        if (err) {
	         	console.log(err);
	            return res.end("Something went wrong!");
	        }
		    getSiteSettingsDetails((site_settings) => {
	            if(site_settings.length == 1){
	                skug_site_settings.update(
	                    {
	                     site_url 			: req.body.site_url,
				         footer_settings 	: req.body.footer_settings,
				         theam_name			: req.body.theam_name,
				         logo				: req.files[0].filename,
						 meta_title			: req.body.meta_title,
						 meta_description 	: req.body.meta_description
	                    },function(err, success) {
	                    // If error arises, i.e. data not inserted
	                    if(err)
	                        console.log(err);
	                    else
	                        console.log("Update success");
	                    res.redirect('/backend/add-site-settings');
	                });
	            }
	            else
	            {
	                skug_site_settings.insertMany(
	                    {
	                     site_url 			: req.body.site_url,
				         footer_settings 	: req.body.footer_settings,
				         theam_name			: req.body.theam_name,
				         logo				: req.files[0].filename,
						 meta_title			: req.body.meta_title,
						 meta_description 	: req.body.meta_description
	                    }, function(err, success) {
	                        // If error arises, i.e. data not inserted
	                    if(err)
	                        console.log(err);
	                    else
	                    console.log("Insert success");
	                    res.redirect('/backend/add-site-settings');
	                });
	            } 
	        });
		});
	}; 
// @--------------------------------------------------------------// End -----//

/////////////////////////////////////////////////////////////////
///                       	functions						 ///
///////////////////////////////////////////////////////////////

// @------------------------------------------------// Start -------//
// callback fucton for fetch site settings details.
	function getSiteSettingsDetails(callback) {
		skug_site_settings.find({},(function(err,  site_settings) {
		    if(err)
		        callback(err)
		    else 
		        callback(site_settings); 
		}));
	}
// @----------------------------------------------------// End -----//]