var express = require('express');
var router  = express.Router();
var registration = require('../controllers/registration');
var delete_page_gallery_image = require('../controllers/delete_page_gallery.js');
var isAuth        = require('../middleware/isAuth');
var login = require('../controllers/login');
var test_php_file = require('../controllers/test_for_php.js');

// router.get('/test-php-code',test_php_file.test_php_code);

var web = require('../controllers/web.js');
// @ -----------------------------------//  Start //-------------------//
// @ --------------------------// Admin Controllers //-----------------//
	var dashboard = require('../controllers/admin_dashboard.js');
	var add_new_page = require('../controllers/add_new_page.js');
	var page_updates = require('../controllers/page_updates.js');
	var page_delete = require('../controllers/page_delete.js');
	var veiw_all_page_contents = require('../controllers/veiw_all_page_contents.js');
	var add_new_post = require('../controllers/add_new_post.js');
	var post_updates = require('../controllers/post_updates.js');
	var delete_post = require('../controllers/delete_post.js');
	var view_all_post_contents = require('../controllers/view_all_post_contents.js');
	var add_post_category  = require('../controllers/add_post_category.js');
	var view_all_post_category  = require('../controllers/view-all-post-category.js');
	var edit_post_category  = require('../controllers/edit_post_category.js');
	var delete_post_category    = require('../controllers/delete_post_category.js');
	var user_registration = require('../controllers/user_registration.js');
	var edit_users_details = require('../controllers/edit_users_details.js');
	var view_all_users = require('../controllers/view_all_users.js');
	var delete_user = require('../controllers/delete_registred_user.js');
	var add_gallery = require('../controllers/add_new_gallery.js');
	var view_gallery = require('../controllers/view-all-gallery.js');
	var edit_gallery_image = require('../controllers/edit_gallery_image.js');
	var delete_gallery_image = require('../controllers/delete_gallery_image.js');
	var add_more_gallery_images = require('../controllers/add_more_gallery_images.js');
	var delete_gallery_contents = require('../controllers/delete_gallery_contents.js');
	var add_new_nav_bar    = require('../controllers/add_new_nav_bar.js');
	var view_all_nav_menu    = require('../controllers/view_all_nav_menu.js');
	var edit_nav_menu    = require('../controllers/edit_nav_menu.js');
	var delete_nav_menu    = require('../controllers/delete_nav_menu.js');
// @ ---------------------------------//  END  //---------------------------//
//Page Operation Beginsn here--------------------------------------------->
	// router.get('/', web.theme_design_call);
	router.get('/', login.log_in_page);
	// router.get('/data-insert-here', isAuth, add_new_page.data_insertForm);
	router.get('/add-site-settings', isAuth, add_new_page.add_site_settings);
	router.post('/add-new-site-settings', add_new_page.add_new_site_settings);
	router.get('/add-new-page', isAuth, add_new_page.add_new_pages);
	router.post('/submit-dataInsert', isAuth, add_new_page.submit_dataInsert);
	router.get('/getFile', add_new_page.getFile);
	router.post('/upload', add_new_page.upload);
	router.post('/delete_file', add_new_page.delete_file);
	router.get('/edit-page', isAuth, page_updates.page_update);
	router.post('/page-data-update', page_updates.pageUpdate_content);
	router.get('/page-delete', isAuth, page_delete.delete_page);
	router.get('/view-all-pages', isAuth , veiw_all_page_contents.view_all_page_data);
	// router.get('/page-data-contents', isAuth, veiw_all_page_contents.pageData_contents);
	// router.get('/frontend/index-page-info', isAuth, veiw_all_page_contents.index_page_info);
//Post router Begins here------------------------------------------------------------------>
	router.get('/add-new-post',add_new_post.add_new_postPage);
	router.post('/add-post', isAuth, add_new_post.add_post_page);
	router.get('/post-update',isAuth, post_updates.post_update);
	router.post('/post-data-update', isAuth, post_updates.post_data_update);
	router.get('/delete-post-page', isAuth, delete_post.delete_post_page);
	router.get('/post-description', isAuth, view_all_post_contents.post_page_description);
	router.get('/view-all-post', isAuth, view_all_post_contents.view_all_post_data);
	router.get('/add-new-category', isAuth, add_post_category.add_new_post_category);
	router.post('/add-new-post-category-submit', isAuth, add_post_category.add_new_post_category_submit);
	router.get('/view-all-category', isAuth, view_all_post_category.view_all_post_category_here);
	router.get('/edit-post-category-page', isAuth, edit_post_category.edit_post_category_page);
	router.post('/edit-post-category-submit', isAuth, edit_post_category.edit_post_category_submit);
	router.get('/delete-post-category', isAuth, delete_post_category.delete_post_category);

//Post routers End here----------------------------------------------------------------------->
	router.post('/dashboard', login.login); 
	router.post('/forget-user-password', login.forget_user_password); 
	router.get('/admin-dashboard', isAuth, dashboard.admin_dashboard);
	

//User Registration routers Begins here------------------------------------------>
	router.get('/add-new-user', isAuth, user_registration.user_registration_here);
	router.post('/user-registration', isAuth, user_registration.user_registration);
    router.get('/edit-user-details', isAuth, edit_users_details.edit_user_details);
    router.post('/edit-user-all-data', isAuth, edit_users_details.edit_user_all_data);
    router.get('/delete-register-user', delete_user.delete_register_user);
	router.get('/view-all-users', isAuth, view_all_users.view_all_registered_users);
//User Registration routers End here------------------------------------------>

// @ --------------------------/  Start /------------------/
//@ Banner router 
	router.get('/add-new-banner-image',add_gallery.add_new_banner_image);
	router.get('/view-all-banners',add_gallery.view_all_banners);
	router.get('/edit-banner-image',add_gallery.edit_banner_image);
	router.get('/delete-banner-images',add_gallery.delete_banner_images);
	router.post('/edit-banner-image-submit',add_gallery.edit_banner_image_submit);
	router.post('/add-banner-form-submit',add_gallery.add_banner_form_submit);
	router.get('/add-more-banner-images',add_gallery.add_more_banner_images);
	router.get('/delete-banner-details',add_gallery.delete_banner_details);
	router.post('/add-one-more-banner-submit',add_gallery.add_one_more_banner_submit);
// @---------------------------/  End ?--------------------/

//All Gallery router Here----------------------------------------------------->
	router.get('/add-new-gallery',add_gallery.add_new_gallery);
	router.post('/submit-gallery', add_gallery.submit_gallery);
	router.post('/submit-add-gallery', add_gallery.submit_add_gallery);
	router.get('/view-all-gallery',view_gallery.view_all_gallery);
	router.get('/edit-gallery-images-img',edit_gallery_image.edit_gallery_images_img);
	router.post('/edit-gallery-images',edit_gallery_image.edit_gallery_images);
	router.get('/delete-gallery-images',delete_gallery_image.delete_gallery_images);
	router.get('/add-more-image',add_more_gallery_images.add_more_image_ejs);
	router.post('/add-more-images-submit',add_more_gallery_images.add_more_images_submit);
	router.get('/delete-gallery-data', delete_gallery_contents.delete_gallery_data);

	// nav bar routers are here------------------------------------------------------------------->
	router.get('/add-new-nav-menu', add_new_nav_bar.nav_bar_page); 
	router.post('/add-nav-bar-submit', add_new_nav_bar.add_nav_bar_submit); 
	router.get('/view-all-nav-menu', view_all_nav_menu.view_all_nav_menu); 
	router.get('/edit-nav-menu-page', edit_nav_menu.edit_nav_menu_page); 
	router.post('/edit-nav-menu-submit', edit_nav_menu.edit_nav_menu_submit); 
	router.get('/delete-nav-menu-page', delete_nav_menu.delete_nav_menu_page); 
	router.post('/custom-nav-bar', add_new_nav_bar.custom_nav_bar);
	router.post('/update-custom-nav-bar', edit_nav_menu.update_custom_nav_bar);
	
	// router.get('/registered-user-list', login.registered_user_list);
	router.get('/log-in', login.log_out);
	
	router.get('/delete-page-gallery-image',delete_page_gallery_image.delete_page_gallery_image);



	

module.exports = router;