var express     = require('express');
var app         = express();
var MongoClient = require('mongodb').MongoClient;
var path        = require('path');
var cookieParser= require('cookie-parser');
var redirect    = require('express-redirect');
var config       =   require('./configuration/config');
// const { port } = require('./configuration/config');
const { endpoint, masterKey, port } = require('./configuration/config');
var session      = require('express-session');
var MongoDBStore =   require('connect-mongodb-session')(session);
var flash       = require('req-flash');
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');
var route       = require('./routes/route');
var router 		= express.Router();

var web = require('./controllers/web.js');


const PORT_NO = process.env.PORT;
const API_KEY = process.env.API_KEY;
var MONGODB_URI = API_KEY;

const ejsLint   = require('ejs-lint');
var app = express();
var session_store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'trpn_active_sessions'
});
    //@ Catch session_store errors
    session_store.on('error', function(error) {
      console.log(error);
    });

redirect(app);
	
	app.use(cookieParser());
	app.use(session({ secret: '123' }));
	app.use(flash());
	app.use(express.static(__dirname+ '/public'));
	app.set('views', path.join(__dirname, '/public'));
	// app.use(express.static(__dirname+ '/backend'));
	// app.set('views', path.join(__dirname, '/backend'));
	app.engine('html', require('ejs').renderFile);
	app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
	app.use(bodyParser.json());                                     // parse application/json
	app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as jso
	app.set('view engine', 'html');
	app.set('view engine', 'ejs');
	app.use(session( {
        name: "tripinn_sid",
        secret: config.session_hash,
        resave: false,
        saveUninitialized: false,
        store: session_store,
        cookie: {
            maxAge: config.session_time,
            sameSite: true
        }
    }))  

	mongoose.connect(MONGODB_URI, { useNewUrlParser: true });
	app.get('/', web.theme_design_call);
	app.use('/backend', route);
	app.get('/:dispaly_id', web.dynamic_page_call);

	// const PORT = 3000;
	const HOST = '0.0.0.0';

	// console.log(`Default environment of an application is: ${app.get('env')}`);
	// console.log(`Default port number of an application is: ${process.env.PORT}`);

	app.listen(PORT_NO,HOST);
console.log(`Running on http://${HOST}:${PORT_NO}`);