// --------------------------------------------- /- start -/
//@ Middleware: To check if there is an already existing 
//@ session .
module.exports = (req, res, next) => {
	if(!req.session.isLoggedIn) {
		// console.log("login success");
		return res.redirect('/backend');
	} else {
		next();
	}
};
// --------------------------------------------- /- end -/