var mongoose = require('mongoose');
const Schema = mongoose.Schema;

	// User and Admin ===================================================================	
	//@ Create a user schema.
var reguserschema = new Schema({
	name		: String,	
	designation : String,  
	email		: String,
	password	: String,
	message 	: String,
	image       : String
});
mongoose.model('registration', reguserschema);

	//@ Create Schema for Add Page.
var add_page_schema = Schema({
	pageName  		: String, 
	// id        		: String,
	URL       		: String,
	featuredImaeg	: String,
	contents        : String, 
	message         : String,
	gallery         : [{}],
	banner_page     : String,
	page_template	: String,
	publish			: String,
	CreateDate      : String
});
mongoose.model('skug_page', add_page_schema);

	//@ Create Schema for Add New page SEO.
var add_page_seo_schema 	= Schema({
	// aouto_Id   				: Number,
	page_name 				: String,
	page_id					: String,
	page_name_seo 			: String,
	seo_title 				: String,
	seo_descriptions 		: String,
	seo_keywords			: String,
	seo_canonicals			: String,
	og_title				: String,
	og_descriptions			: String,
	og_urls					: String,
	og_types				: String,
	og_images				: String,
	og_site_names			: String,
	twitter_titles			: String,
	twitter_descriptions	: String,
	twitter_cards			: String,
	twitter_sites			: String,
	twitter_creators		: String
});
mongoose.model('skug_page_seo', add_page_seo_schema);

	// @ Create Schema for Add New Post Here.
var add_new_post_schema = new Schema({
	postName			: String, 
	post_url			: String,
	featured_imaeg		: String,
	contents			: String, 
	post_description	: String,
	gallery				: [{}],
	publish			    : String,
	createDate 			: Date
});
mongoose.model('skug_post', add_new_post_schema);

	// @ Create Schema for Add New Post Seo.
var add_new_post_seo_schema  = Schema({
	post_name 				 :String,
	seo_title				 : String,
	seo_descriptions		 : String,
	seo_keywords			 : String,
	seo_canonicals			 : String,
	og_title				 : String,
	og_descriptions			 : String,
	og_urls					 : String,
	og_types				 : String,
	og_images				 : String,
	og_site_names			 : String,
	twitter_titles			 : String,
	twitter_descriptions	 : String,
	twitter_cards			 : String,
	twitter_sites			 : String,
	twitter_creators		 : String
});
mongoose.model('skug_post_seo', add_new_post_seo_schema);

	//@ Create Schema for Category Relations.
var category_relation = Schema({
	post_id 			: String,
	postName			: String, 
	post_url			: String,
	post_category 			: String,
	post_featured_imaeg  :String
});
mongoose.model('skug_category_relation',category_relation)

	// @ Create Schema for Add New Gallery for page and Post.
var gallery_schema  = Schema({
	primary_id		: Number,
	title 			: String,
	type 			: String,
	ptype 			: String,
	gallery 		: [{}]
}, { versionKey: false });
mongoose.model('skug_gallery', gallery_schema);

	// @ Create Schema for Add New User Or Registeration New User And User Role.
var add_new_user_role_schema  = Schema({
	first_name				  : String,
	last_name				  : String,
	user_id					  : String,
	password				  : String,
	email					  : String, 
	mobile					  : String,
	user_role  				  : String,
	image 					  : String 
});
mongoose.model('skug_user', add_new_user_role_schema);

	// @ Create Schema for add new post category.
var post_category         		 = Schema({
	category_name             	 : String,
	category_slug			  	 : String,
	category_description	  	 : String,
	cat_seo_title				 : String,
	cat_seo_descriptions		 : String,
	cat_seo_keywords			 : String,
	cat_seo_canonicals			 : String,
	cat_og_title				 : String,
	cat_og_descriptions			 : String,
	cat_og_urls					 : String,
	cat_og_types				 : String,
	cat_og_images				 : String,
	cat_og_site_names			 : String,
	cat_twitter_titles			 : String,
	cat_twitter_descriptions	 : String,
	cat_twitter_cards			 : String,
	cat_twitter_sites			 : String,
	cat_twitter_creators		 : String
});
mongoose.model('skug_post_category', post_category);

	// @ Create Schema for add new nav menu.
var nav_bar_menu         = Schema({
	nav_title            : String,
	custom_menu          : [{}],
	custom_nav_menu_link : [{}],
	post_category        : [{}],
	// category_slug		 : [{}],
	page_name            : Object,
	page_slug			 : Object,
	post_name		     : [{}],
	gallery_title        : [{}]
});
mongoose.model('skug_nav_menu', nav_bar_menu);

// @ For Custom nav menu.
var custom_nav_menu     = Schema({
	custom_menu         : String,
	custom_menu_link    : String
});
mongoose.model('custom_menues',custom_nav_menu);

// @ --------------------------/  Start /------------------/ 
// @ for site settings models.
var site_settings_schema = Schema({
	site_url 			: String,
	footer_settings     : String,
	theam_name 			: String,
	logo				: String,
	meta_title			: String,
	meta_description 	: String
});
mongoose.model('skug_site_settings',site_settings_schema);
// @---------------------------/  End ?--------------------/

// @ --------------------------/  Start /------------------/ 
// @ for banner data models.
	var banner_details = Schema({
		banner_title   : String,
		banner_files   : [{}]
	});
	mongoose.model('banner_details',banner_details);
// @---------------------------/  End ?--------------------/
